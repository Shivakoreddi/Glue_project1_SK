

import sys
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from awsglue.dynamicframe import DynamicFrame

## @params: [JOB_NAME]1
args = getResolvedOptions(sys.argv, ['JOB_NAME'])

sc = SparkContext()
glueContext = GlueContext(sc)
spark = glueContext.spark_session
job = Job(glueContext)
job.init(args['JOB_NAME'], args)

# Input data location
s3_input_path = "s3://gluesources/usersdata/"

# Output data location
s3_output_path = "s3://gluetargets/userDataAnalytics"

# Read data from the input location using a Glue DynamicFrame
datasource0 = glueContext.create_dynamic_frame_from_options(
    "s3", {'paths': [s3_input_path]}, format="parquet"
)

# Convert DynamicFrame to a DataFrame for transformations
df = datasource0.toDF()

# Perform transformations on the DataFrame
# For example, you can apply a filter or any other DataFrame operations
transformed_df = df.filter(df["salary"] > 5000)

# Convert the transformed DataFrame back to a DynamicFrame
transformed_datasource = DynamicFrame.fromDF(transformed_df, glueContext, "transformed_datasource")

# Write the transformed data back to S3
glueContext.write_dynamic_frame.from_options(
    frame=transformed_datasource,
    connection_type="s3",
    connection_options={"path": s3_output_path},
    format="csv",
)

job.commit()
