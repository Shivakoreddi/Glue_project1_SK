import sys
from pyspark.context import SparkContext
from pyspark.sql import SQLContext
from awsglue.utils import getResolvedOptions
from awsglue.context import GlueContext
from awsglue.dynamicframe import DynamicFrame
from awsglue.job import Job
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, TimestampType, DoubleType


## @params: [JOB_NAME]1
args = getResolvedOptions(sys.argv, ['JOB_NAME'])


sc = SparkContext()
glueContext = GlueContext(sc)
job = Job(glueContext)
job.init(args['JOB_NAME'],args)
sqlContext = SQLContext(sc)

#read source data

src_user_dyf = glueContext.create_dynamic_frame_from_options(
    connection_type="s3",
    format="parquet",
    connection_options = {"path":["s3://gluesources/userdata/"]}
)
tgt_user_dyf = glueContext.create_dynamic_frame_from_options(
    connection_type="s3",
    format="parquet",
    connection_options = {"path":["s3://gluetargets/userData_inc/"]}
)

schema_fields = [
    StructField("registration_dttm", TimestampType(), True),
    StructField("id", IntegerType(), True),
    StructField("first_name", StringType(), True),
    StructField("last_name", StringType(), True),
    StructField("email", StringType(), True),
    StructField("gender", StringType(), True),
    StructField("ip_address", StringType(), True),
    StructField("cc", StringType(), True),
    StructField("country", StringType(), True),
    StructField("birthdate", StringType(), True),  # Assuming birthdate is stored as string
    StructField("salary", DoubleType(), True),
    StructField("title", StringType(), True),
    StructField("comments", StringType(), True)
]

##convert to schema
schema = StructType(schema_fields)

#convert dynamicframe to dataframe
src_user_df = sqlContext.createDataFrame(src_user_dyf.toDF().rdd,schema)
tgt_user_df = sqlContext.createDataFrame(tgt_user_dyf.toDF().rdd,schema)

#filter dataframe 

max_timestamp = tgt_user_df.agg({"registration_dttm":"max"}).collect[0][0]
new_dataset_df = src_user_df.filter(src_user_df.registration_dttm>max_timestamp)


#apply necessary transformation on df

#append new records 
upd_tgt_df = tgt_user_df.union(new_dataset_df)

#convert the updated dataframe back to dynamic frame

upd_tgt_dyf = DynamicFrame.fromDF(upd_tgt_df,glueContext,"updated_target")

#write updated target back to storage s3

glueContext.write_dynamic_frame.from_options(
    frame = upd_tgt_dyf,
    connection_type="s3",
    format="csv",
    connection_options={"path":["s3://gluetargets/userData_inc/"]}
    
)
job.commit()
