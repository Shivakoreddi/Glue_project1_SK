import csv 
import boto3
import pandas as pd

def lambda_handler(event,context):
    
    #extract bucket and key
    bucket = event['Records'][0]['s3']['bucket']['name']
    key = event['Records'][0]['s3']['object']['key']
    ##read from s3
    s3 = boto3.client('s3')
    csv_file = f"/tmp/{key.split('/')[-1]}"
    s3.download_file(bucket,key,csv_file)
    df = pd.read_csv(csv_file)
    
    parquet_file = "/tmp/data.parquet"
    df.to_parquet(parquet_file)
    tgt_bucket = "s3://gluesources"
    tgt_key = f"{key.split('/')[-1]}.parquet"
    s3.upload_file(parquet_file,tgt_bucket,tgt_key)
    return {'satusCode':200, 'body':'File conversion Successful'}
