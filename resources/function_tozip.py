import os
import zipfile

def create_lambda_zip(lambda_dir, zip_file):
    # Initialize a ZIP file for writing
    with zipfile.ZipFile(zip_file, 'w') as zipf:
        # Walk through the directory and add each file to the ZIP file
        lambda_dir_path = os.path.expanduser(lambda_dir)
        for root, dirs, files in os.walk(lambda_dir_path):
            for file in files:
                file_path = os.path.join(root, file)
                zip_path = os.path.relpath(file_path, lambda_dir_path)
                zipf.write(file_path, zip_path)

# Example usage:
lambda_dir = '~/Documents/Glue_project1_SK/lambda'
zip_file = 'rawtocurated.zip'

create_lambda_zip(lambda_dir, zip_file)
print(f"Lambda function ZIP file '{zip_file}' created successfully.")