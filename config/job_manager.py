import boto3
import yaml
import os

def create_or_update_glue_job(job_properties):
    client = boto3.client('glue', region_name='us-west-1',aws_access_key_id='AKIAXGMDTB5E7JQQ5CW4', aws_secret_access_key='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')

    try:
        response = client.get_job(JobName=job_properties['Name'])
        print("Job already exists, updating...")
        response = client.update_job(JobName=job_properties['Name'], JobUpdate={
                'Role': job_properties['Role'],
                'GlueVersion': job_properties['GlueVersion'],
                'Command': {
                    'Name': job_properties['Command']['Name'],
                    'ScriptLocation': job_properties['Command']['ScriptLocation'],
                    
                },
                'DefaultArguments': job_properties['DefaultArguments'],
                'Timeout': job_properties['Timeout'],
                
            })
        print("Job updated successfully.")
    except client.exceptions.EntityNotFoundException:
        print("Job not found, creating...")
        response = client.create_job(**job_properties)
        print("Job created successfully.")

    return response

def create_update_lambda(job_properties):
    client = boto3.client('lambda', region_name='us-west-1',aws_access_key_id='AKIAXGMDTB5E7JQQ5CW4', aws_secret_access_key='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')
    try:
        print(job_properties['Function_Name'])
        client.get_function(FunctionName = job_properties['Function_Name'])
        response = client.update_function_code(FunctionName = job_properties['Function_Name'],ZipFile = open(job_properties['zip_file_path'],'rb').read())
        print(f"Lambda Function updated successfully")
    except client.exceptions.ResourceNotFoundException:
        response = client.create_function(FunctionName=job_properties['Function_Name'],
            Runtime=job_properties['Runtime'],
            Role=job_properties['Role_ARN'],
            Handler=job_properties['handler'],
            Code={
                'ZipFile': open(job_properties['zip_file_path'], 'rb').read(),
            },)
        print("Lambda created successfully.")
    return response
    
    
         

with open('config/param.yml', 'r') as file:
    all_job_properties = yaml.safe_load(file)

for job_properties in all_job_properties:
    if 'Name' in job_properties:
        create_or_update_glue_job(job_properties)
    
    elif 'Function_Name' in job_properties:
        zip_file_path = os.path.expanduser(job_properties['zip_file_path'])
        # Update the job_properties with the expanded zip_file_path
        job_properties['zip_file_path'] = zip_file_path
        create_update_lambda(job_properties)
    else:
        print("Error: Unrecognized job property entry: ",job_properties)
